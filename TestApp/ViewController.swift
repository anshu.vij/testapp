//
//  ViewController.swift
//  TestApp
//
//  Created by Anshu Vij on 09/12/21.
//

import UIKit
import OhmFramework



class ViewController: UIViewController {

    @IBOutlet weak var tokenField: UITextView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        navigationController?.setNavigationBarHidden(true, animated: true)
    }

    @IBAction func loginPressed(_ sender: UIButton) {
    

        let controller = AppLauncherVC()
        controller.credential.MAIN_URL = "https://prod.wiser.sustlabs.com/getToken?BFO="+(tokenField.text)
        
        print(controller.credential.MAIN_URL)
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
}

