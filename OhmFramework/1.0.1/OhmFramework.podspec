#
#  Be sure to run `pod spec lint OhmFramework.podspec' to ensure this is a
#  valid spec and to remove all comments including this before submitting the spec.
#
#  To learn more about Podspec attributes see https://guides.cocoapods.org/syntax/podspec.html
#  To see working Podspecs in the CocoaPods repo see https://github.com/CocoaPods/Specs/
#

Pod::Spec.new do |spec|

  # ―――  Spec Metadata  ―――――――――――――――――――――――――――――――――――――――――――――――――――――――――― #
  #
  #  These will help people to find your library, and whilst it
  #  can feel like a chore to fill in it's definitely to your advantage. The
  #  summary should be tweet-length, and the description more in depth.
  #

  spec.name         = "OhmFramework"
  spec.version      = "1.0.1"
  spec.summary      = "OhmFramework library for Schneider"
  spec.description  = "OhmFramework library for Schneider integration"
  spec.homepage     = "https://gitlab.com/anshu.vij/ohmframeworklib"
  spec.license      = "MIT"
  spec.author             = { "Anshu Vij" => "anshu.vij@sustlabs.com" }
  spec.platform     = :ios
  spec.ios.deployment_target = "13.4"
  spec.source       = { :git => "https://gitlab.com/anshu.vij/ohmframeworklib.git", :tag => spec.version.to_s }
  spec.source_files  = "OhmFramework", "OhmFramework/**/*.{swift}"
  spec.frameworks = "UIKit", "CoreBluetooth", "WebKit"
  # ――― Resources ―――――――――――――――――――――――――――――――――――――――――――――――――――――――――――――――― #
  #
  #  A list of resources included with the Pod. These are copied into the
  #  target bundle with a build phase script. Anything else will be cleaned.
  #  You can preserve files from being cleaned, please don't preserve
  #  non-essential files like tests, examples and documentation.
  #

  # spec.resource  = "icon.png"
  # spec.resources = "Resources/*.png"

  # spec.preserve_paths = "FilesToSave", "MoreFilesToSave"



  # ――― Project Settings ――――――――――――――――――――――――――――――――――――――――――――――――――――――――― #
  #
  #  If your library depends on compiler flags you can set them in the xcconfig hash
  #  where they will only apply to your library. If you depend on other Podspecs
  #  you can include multiple dependencies to ensure it works.

  # spec.requires_arc = true

  # spec.xcconfig = { "HEADER_SEARCH_PATHS" => "$(SDKROOT)/usr/include/libxml2" }
  # spec.dependency "JSONKit", "~> 1.4"
    
  spec.subspec 'Core' do |cs|
      cs.dependency "ESPProvision", "~> 2.0.14"
      cs.dependency "MBProgressHUD", "~> 1.2.0"
  end
  
  spec.swift_versions = "5.0"

end
